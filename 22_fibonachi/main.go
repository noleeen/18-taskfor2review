//22 Параллельное вычисление чисел Фибоначчи:
//Реализуйте функцию, которая принимает число N и возвращает N-е число Фибоначчи.
//Запустите несколько горутин, которые будут параллельно вычислять числа Фибоначчи для разных значений N
//и выводить результаты на экран.

package main

import "fmt"

func fib(num int) <-chan int {
	c := make(chan int, num)

	go func() {
		i, j := 1, 1
		c <- i
		for a := 0; a < num-1; a++ {
			c <- i
			i, j = i+j, i
		}
		close(c)
	}()

	return c
}

func main() {
	N := 21

	res := make([]int, 0, N)
	for fn := range fib(N) {
		res = append(res, fn)
	}
	fmt.Println(res[N-1])
	fmt.Println(res)
}
