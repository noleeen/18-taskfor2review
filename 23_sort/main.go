//Реализуйте параллельную сортировку массива с помощью алгоритма сортировки слиянием.
//Разделите массив на две равные части, и запустите две горутины для сортировки каждой из частей.
//Затем объедините отсортированные части с помощью еще одной горутины.
package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func NewSlice(lenS int) []int {
	s := make([]int, lenS)
	for i := 0; i < len(s); i++ {
		s[i] = rand.Intn(1000)
	}

	return s
}

func mergeSlices(r, l []int) (res []int) {
	res = make([]int, 0, len(r)+len(l))
	i, j := 0, 0
	for i < len(r) && j < len(l) {
		if r[i] < l[j] {
			res = append(res, r[i])
			i++
		} else {
			res = append(res, l[j])
			j++
		}
	}

	if i < len(r) {
		res = append(res, r[i:]...)
	} else if j < len(l) {
		res = append(res, l[j:]...)
	}
	return
}

func sortSlice(s []int) []int {
	if len(s) <= 1 {
		return s
	}
	middle := len(s) / 2
	l := s[:middle]
	r := s[middle:]

	newR := make([]int, 0)
	newL := make([]int, 0)
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		newL = sortSlice(l)
		wg.Done()
	}()
	go func() {
		newR = sortSlice(r)
		wg.Done()
	}()
	wg.Wait()
	resSortSlice := make([]int, 0)
	wg.Add(1)
	go func() {
		resSortSlice = mergeSlices(newR, newL)
		wg.Done()
	}()
	wg.Wait()

	return resSortSlice
}

func main() {
	rand.Seed(time.Now().UnixNano())
	f := NewSlice(33)
	fmt.Println(f)

	sortMySlice := sortSlice(f)
	fmt.Println(sortMySlice)

}
