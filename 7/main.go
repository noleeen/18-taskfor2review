//Напишите программу, которая создает канал и несколько горутин,
//каждая из которых отправляет на канал случайное число.
//Одна горутина должна получать значения из канала и выводить их на экран,
//а другая должна подсчитывать сумму полученных значений.
//Когда все значения будут получены, программа должна вывести их сумму на экран.

package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	ch := make(chan int)
	tempCh := make(chan int)
	sum := 0
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 3; i++ {
			ch <- rand.Intn(10)
			fmt.Println("sender1")
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 3; i++ {
			ch <- rand.Intn(10)
			fmt.Println("sender2")
		}
	}()
	go func() {
		wg.Wait()
		close(ch)
	}()

	go func() {

		for d := range ch {
			fmt.Println(d)
			tempCh <- d

		}
		close(tempCh)

	}()

	go func() {
		for s := range tempCh {
			sum += s
			fmt.Println("temtSum = ", sum)

		}
	}()

	runtime.Gosched()
	fmt.Println("sum= ", sum)
}
