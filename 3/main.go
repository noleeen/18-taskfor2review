//Напишите программу, которая создает канал и несколько горутин, которые получают значения из канала.
//Когда все значения будут получены, программа должна закрыть канал.

package main

import (
	"fmt"
	"sync"
)

func main() {
	ch := make(chan int)
	//t := make(chan struct{})
	wg := sync.WaitGroup{}

	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			ch <- i
			fmt.Println("write")
		}
		close(ch)
	}()

	for i := 0; i < 2; i++ {
		go func() {
			defer wg.Done()
			for r := range ch {
				fmt.Println(r)
			}
		}()
	}
	wg.Wait()
}
