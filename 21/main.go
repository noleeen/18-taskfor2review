//21 Производитель-потребитель:
//Создайте две горутины, одна будет производителем (producer), а другая потребителем (consumer).
//Производитель будет генерировать случайные числа и отправлять их через канал потребителю.
//Потребитель будет выводить полученные числа на экран.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//wg := sync.WaitGroup{}
	rand.Seed(time.Now().UnixNano())
	ch := make(chan int)

	go func() {
		for {
			time.Sleep(time.Second)
			ch <- rand.Intn(1000)
		}
	}()

	go func() {
		for {
			fmt.Println(<-ch)
		}
	}()

	//for {
	//	fmt.Println(<-ch)
	//}

	time.Sleep(time.Second * 30)
	fmt.Println("finish")

}
