//Напишите программу, которая создает канал и несколько горутин, которые отправляют значения в канал.
//Когда все значения будут отправлены, программа должна закрыть канал и вывести все полученные значения.

package main

import (
	"fmt"
	"sync"
)

func main() {
	ch := make(chan int)

	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			ch <- i
		}
	}()
	go func() {
		defer wg.Done()
		for i := 15; i < 20; i++ {
			ch <- i
		}
	}()

	go func() {
		wg.Wait()
		close(ch)
	}()

	for v := range ch {

		fmt.Println(v)
	}
}
