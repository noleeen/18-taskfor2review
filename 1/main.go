//Напишите программу, которая создает 2 канала и передает значения между ними.
//Передача должна осуществляться в обе стороны.

package main

import "fmt"

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go sendData(76, ch1)
	go chToCh(ch1, ch2)
	fmt.Println(<-ch2)
	go sendData(79, ch1)
	//go chToCh(ch1, ch2)
	fmt.Println(<-ch1)
}

func sendData(data int, c chan int) {
	c <- data
}

func chToCh(c chan int, d chan int) {
	temp := <-c
	d <- temp
}
