//Напишите программу, которая создает 2 канала и несколько горутин.
//Первая горутина отправляет значения в первый канал, а вторая горутина получает значения из второго канала.
//Когда все значения будут отправлены и получены, программа должна закрыть оба канала.
package main

import (
	"fmt"
	"runtime"
)

func main() {
	ch1 := make(chan int, 10)
	ch2 := make(chan int, 03)
	ch2 <- 111
	ch2 <- 222
	ch2 <- 333

	go func() {
		defer close(ch1)

		for i := 0; i < 10; i++ {
			ch1 <- i * 5
			fmt.Println("len(ch1)", len(ch1))
		}
		fmt.Println("!len(ch1)", len(ch1))
	}()

	go func() {
		defer close(ch2)
		defer fmt.Println("len(ch2)", len(ch2)) // не понимаю, почему эта строка не выводится!?
		for c := range ch2 {
			fmt.Println(c)

		}
	}()
	runtime.Gosched()
	fmt.Println("fin len", len(ch2), len(ch1))

}
